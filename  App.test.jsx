import { describe, it, expect } from 'vitest';
import capitalize from "./src/pages/Home/capitalize"

describe('Convert first latter in name', () => {
  it('firstName  letter is uppercase ', () => {
    expect(capitalize("leo")).toBe("Leo") 
  });

});